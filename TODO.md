# TODO List

These are mostly just thoughts on things that could/should be done at
some point.

## Components

Convert to lists of components and lists of resources to indexed list
for speedier lookups

Merge rules support for components in perl

## Packages

Either converted to indexed lists or add hash support for speedier
lookup for things that need it (e.g. updaterpms).

Add rpmlist and package fetch support with curl.

## Resources

tags - add unique, add set functions - union, difference, intersection

## Utils

Improve code quality.

## Common

Expose common constants in Perl - macros/functions for checking value?
